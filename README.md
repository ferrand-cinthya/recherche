Comment faire une recherche de commande inconnue ? ("Je recherche une commande qui permet de ..." exemple la commande qui permet de recupere tous les filchers d'un document en python)

Trouver l'éléments précis dans une documentation officiel? (exemeple integre une liste de dictionaire dans une table sqlite3 )

Passée d'un exemple non adaptée à la commande rechercher (j'ai trouver une reponse sur stackoverflow mais elle ne fonctionne pas chez moi pourquoi que doit-je changer ?).


# Où recherche des informations

Etape 1 : faites un remue-méninges sur la question posée.

Posez-vous les six questions de base : 3QOCP ? (Qui ? Quoi ? Quand ? Où ? Comment ? Pourquoi ?). Cela vous permettra de trouver de nouveaux mots-clés.

Oubliez les mots de liaison inutiles, les mots vides (de, je, les, une, pour…), les phrases longues ou les questions telles que « combien coûte une amende pour non port de la ceinture de sécurité ? ». Vous risquez de brouiller les pistes. Privilégiez plutôt « Amende non port ceinture ». Vos résultats de recherche correspondent à la qualité de votre question : Faites simple, concis et vous aurez la réponse en quelques clics.


Etape 2 : utilisez des opérateurs de recherche
https://www.louismaitreau.fr/blog/recherche-efficace-google (tableau opérateur de recherche complet)

Pour réduire le nombre de résultats, il faut donc :

- Sélectionner les mots-clés.

- Rédiger la formule de recherche possible que vous taperiez sur Google.

- « Aucune réponse ne correspond à vos critères » et vous voulez plus de résultats ? Il faudra alors    supprimer des mots, des opérateurs ou trouver des synonymes !

# Valider l’information

Une fois l'information trouver ce servir des mots-clefs ou des termes précis pour multiplier les resultats et confirmer l'utilisation.











